package pl.sggw.wzim.server;

import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;
import java.util.List;

import pl.sggw.wzim.main.Receiver;
import pl.sggw.wzim.utils.Utils;

public class Server {
	
	public static final int SERVER_PORT_NUMBER = 2034;

	private List<Socket> sockets;
	private List<Receiver> livingReceivers;
	
	ServerSocket serverSocket;
	
	public float sumResult() {
		float sum = 0;
		for (Receiver receiver : livingReceivers) {
			sum += receiver.getResult();
		}
		
		System.out.println("Calculated sum from results: " + sum);
		return sum;
	}
	
	public void waitForResult() {
		while(true) {
			if (isResultReady()) break;
			
			try {
				Thread.sleep(100);
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
		
		int i = 1;
		for (Receiver receiver : livingReceivers) {
			System.out.println("Recieved " + i + " Result: " + receiver.getResult());
			i++;
		}
	}
	
	private boolean isResultReady() {
		for (Receiver receiver : livingReceivers) {
			if (receiver.isAlive()) {
				return false;
			}
		}
		return true;
	}
	
	private void startListenForResult(Socket socket) {
		//Start waitng for response
		Receiver r = new Receiver(socket);
		r.start();
		livingReceivers.add(r);
	}
	
	public void sendToClients() {
		System.out.println("Start sending data");

		List<String> intervals = Utils.getIntervals(sockets.size());
		int i = 0;

		for (Socket socket : sockets) {
			try {
				DataOutputStream out = new DataOutputStream(socket.getOutputStream());

				System.out.println("Sending Intervals: " + intervals.get(i));

				out.writeUTF(intervals.get(i));

				startListenForResult(socket);
				i++;
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	
	public void runAndWaitForConnections(int connectionsCount) {
		try {
			serverSocket = new ServerSocket(SERVER_PORT_NUMBER);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		System.out.println("Start");
		
		for (int i = 0; i < connectionsCount; i ++) {
			try {
				Socket s = serverSocket.accept();
				
				System.out.println("Connected: " + (i + 1));
				
				sockets.add(s);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			
		}
	}
	
	public Server() {
		this.sockets = new ArrayList<>();
		this.livingReceivers = new ArrayList<>();
	}
}
