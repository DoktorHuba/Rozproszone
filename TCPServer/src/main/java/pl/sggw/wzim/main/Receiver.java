package pl.sggw.wzim.main;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;

public class Receiver extends Thread {

	private Socket socket;
	private Float result;
	
	public Receiver(Socket s) {
		this.socket = s;
	}
	
	@Override
	public void run() {
		try {
			DataInputStream in = new DataInputStream(socket.getInputStream());
			float f = in.readFloat();
			this.result = f;
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}

	public Float getResult() {
		return result;
	}
	
	
}
