package pl.sggw.wzim.main;


import pl.sggw.wzim.server.Server;
import pl.sggw.wzim.utils.Utils;

public class Main {
	
	public static final float BEGIN_X = 0f;
	public static final float END_X = 1f;

	public static void main(String[] args) {
		
		//TODO get from console args
		int clients = 2;

		Server server = new Server();
		server.runAndWaitForConnections(clients);
		server.sendToClients();
		server.waitForResult();
		server.sumResult();
	}
}
