package pl.sggw.wzim.utils;

import java.util.ArrayList;
import java.util.List;

import pl.sggw.wzim.main.Main;

public class Utils {

	public static List<String> getIntervals(int intervalsCount) {
		System.out.println("Generate Intervals: " + intervalsCount);
		
		List<String> intervals = new ArrayList<>();
		
		Float step = (Main.END_X - Main.BEGIN_X) / intervalsCount;
		Float previous = 0f;
		Float next = null;
		
		for(int i = 0; i < intervalsCount; i++) {
			next = previous + step;
			intervals.add(previous + ":" + next);
			previous = next;
		}
		
		
		return intervals;
	}
}
