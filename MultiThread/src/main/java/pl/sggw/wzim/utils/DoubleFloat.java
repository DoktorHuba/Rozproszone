package pl.sggw.wzim.utils;

public class DoubleFloat {

	private float beginX;
	private float endX;
	
	public DoubleFloat(float beginX, float beginY) {
		super();
		this.beginX = beginX;
		this.endX = beginY;
	}
	
	public float getBeginX() {
		return beginX;
	}
	
	public float getEndX() {
		return endX;
	}
}
