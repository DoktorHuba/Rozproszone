package pl.sggw.wzim.utils;

import java.util.ArrayList;
import java.util.List;

import pl.sggw.wzim.Main;


public class Utils {

	public static List<DoubleFloat> getIntervals(int intervalsCount) {
		System.out.println("Generate Intervals: " + intervalsCount);
		
		List<DoubleFloat> intervals = new ArrayList<>();
		
		Float step = (Main.END_X - Main.BEGIN_X) / intervalsCount;
		Float previous = 0f;
		Float next = null;
		
		for(int i = 0; i < intervalsCount; i++) {
			next = previous + step;
			intervals.add(new DoubleFloat(previous, next));
			previous = next;
		}
		
		
		return intervals;
	}
}
