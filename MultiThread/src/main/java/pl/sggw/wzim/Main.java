package pl.sggw.wzim;

import java.util.ArrayList;
import java.util.List;

import pl.sggw.wzim.math.CalculatingThread;
import pl.sggw.wzim.utils.DoubleFloat;
import pl.sggw.wzim.utils.Utils;

public class Main {

	public static final float BEGIN_X = 0f;
	public static final float END_X= 1f;
	
	public static List<CalculatingThread> threads = null;
	
	public static void main(String[] args) {
		
		int calculatingThreadsCount = 2;
		
		List<DoubleFloat> intervals = Utils.getIntervals(calculatingThreadsCount);
		threads = new ArrayList<>();
		
		for (DoubleFloat doubleFloat : intervals) {
			CalculatingThread thread = new CalculatingThread(doubleFloat.getBeginX(), doubleFloat.getEndX());
			threads.add(thread);
			thread.start();
		}
		
		while(true) {
			if (isResultReady()) {
				break;
			}
		}
		
		float sum = 0;
		for (CalculatingThread thread : threads) {
			sum += thread.getResult();
		}
		
		System.out.println("Result: " + sum);
	}
	
	private static boolean isResultReady() {
		for (CalculatingThread calculatingThread : threads) {
			if (calculatingThread.isAlive()) {
				return false;
			}
		}
		return true;
	}
}
