package pl.sggw.wzim.math;

public class CalculatingThread extends Thread {

	private float beginX;
	private float endX;
	private float result;
	
	public CalculatingThread(float x1, float x2) {
		this.beginX = x1;
		this.endX = x2;
	}
	
	@Override
	public void run() {
		System.out.println(this.getName() + "started calculating from " + beginX + " to " + endX);
		
		this.result = Calculator.countIntegral(beginX, endX);
		
		try {
			Thread.sleep((long) (10000 * endX));
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		System.out.println(this.getName() + "calculated result: " + this.result);
	}

	public float getBeginX() {
		return beginX;
	}

	public float getEndX() {
		return endX;
	}

	public float getResult() {
		return result;
	}
	
	
}
