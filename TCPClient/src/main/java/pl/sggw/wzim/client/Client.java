package pl.sggw.wzim.client;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;

import pl.sggw.wzim.math.Calculator;
import pl.sggw.wzim.utils.DoubleFloat;
import pl.sggw.wzim.utils.Utils;

public class Client {

	public static final int SERVER_PORT_NUMBER = 2034;

	private Socket socket;
	private DoubleFloat limits;
	private float result;

	public Client() {
		try {
			socket = new Socket("localhost", SERVER_PORT_NUMBER);
			
			System.out.println("Connected to server");
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void sendResultToServer() {
		try {
			DataOutputStream out = new DataOutputStream(socket.getOutputStream());
			out.writeFloat(this.result);
			
			System.out.println("Sended result: " + this.result + " to server.");
			socket.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void calculatePartOfIntegral() {
		this.result = Calculator.countIntegral(limits.getBeginX(), limits.getBeginY());
		
		System.out.println("I calculated: " + this.result);
	}

	public void getIntervalsFromServer() {
		try {
			System.out.println("Waiting for interval");
			
			InputStream inFromServer = socket.getInputStream();
			DataInputStream in = new DataInputStream(inFromServer);

			String interval = in.readUTF();

			this.limits = Utils.parseInterval(interval);
			
			System.out.println("I recieved interval from: " + limits.getBeginX() + " to: " + limits.getBeginY());
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}
