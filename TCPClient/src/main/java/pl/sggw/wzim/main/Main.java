package pl.sggw.wzim.main;

import java.io.DataInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.Socket;
import java.net.UnknownHostException;

import pl.sggw.wzim.client.Client;

public class Main {

	public static void main(String[] args) {

		Client client = new Client();
		client.getIntervalsFromServer();
		client.calculatePartOfIntegral();
		client.sendResultToServer();
	}

}
