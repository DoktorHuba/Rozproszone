package pl.sggw.wzim.utils;

public class Utils {

	public static DoubleFloat parseInterval(String string) {
		String[] strings = string.split(":");
		return new DoubleFloat(Float.parseFloat(strings[0]), Float.parseFloat(strings[1]));
	}
}
