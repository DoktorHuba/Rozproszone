package pl.sggw.wzim.utils;

public class DoubleFloat {

	private float beginX;
	private float beginY;
	
	public DoubleFloat(float beginX, float beginY) {
		super();
		this.beginX = beginX;
		this.beginY = beginY;
	}
	
	public float getBeginX() {
		return beginX;
	}
	
	public float getBeginY() {
		return beginY;
	}
}
